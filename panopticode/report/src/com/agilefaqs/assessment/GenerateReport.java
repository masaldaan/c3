package com.agilefaqs.assessment;

import static java.lang.Integer.parseInt;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GenerateReport {

    private static final String LOCATION = "..";
    private static final Map<Integer, String> longMethodMsg = new TreeMap<Integer, String>() {
        {
            put(11, "<=10 - Good");
            put(26, "<=25 - Watch Out");
            put(51, "<=50 - Scary");
            put(101, "<=100 - Danger");
            put(1000, "Blindly Delete It");
        }
    };
    private static final Map<Integer, String> longTestMethodMsg = new TreeMap<Integer, String>() {
        {
            put(11, "<=10 - Good");
            put(21, "<=20 - Watch Out");
            put(31, "<=30 - Scary");
            put(51, "<=50 - Danger");
            put(1000, "Blindly Delete It");
        }
    };
    private static final Map<Integer, String> compexlityMsg = new TreeMap<Integer, String>() {
        {
            put(6, "<=5 - Good");
            put(8, "<=7 - Watch Out");
            put(11, "<=10 - Scary");
            put(26, "<=25 - Danger");
            put(1000, "Blindly Delete It");
        }
    };

    public static void main(String[] args) throws Exception {
        if (args.length == 1 && args[0].equalsIgnoreCase("header"))
            writeHeader();
        else if (args.length == 1 && args[0].equalsIgnoreCase("footer"))
            writeFooter();
        else if (args.length == 2)
            generateReport(args[0], args[1]);
        else {
            writeHeader();
            generateReport("Naresh", new File(LOCATION).getAbsolutePath());
            writeFooter();
        }
    }

    private static void generateReport(String name, String path) throws Exception, IOException {
        StringBuilder result = new StringBuilder();
        GenerateReport q = new GenerateReport();

        int missedCoverage = parseInt(q.fetchSpecificValue("jacoco.xml", "/report/counter[@type='INSTRUCTION']/@missed"));
        int coveredCoverage = parseInt(q.fetchSpecificValue("jacoco.xml", "/report/counter[@type='INSTRUCTION']/@covered"));
        int codeCoveragePercentage = coveredCoverage * 100 / (missedCoverage + coveredCoverage);
        int numberOfTestClasses = parseInt(q.fetchSpecificValue("javancss_test.xml", "//packages/total/classes/text()"));
        int totalTestLoC = parseInt(q.fetchSpecificValue("javancss_test.xml", "//packages/total/ncss/text()"));
        int totalTestMethod = parseInt(q.fetchSpecificValue("javancss_test.xml", "//packages/total/functions/text()"));
        int longestTestMethod = q.fetchHighestValue("javancss_test.xml", "//functions/function/ncss/text()");

        String unitTestingSkills = "Unit Test Quality";
        String xUnitKnowledge = "Knowledge of xUnit";

        if (codeCoveragePercentage < 2) {
            unitTestingSkills = "Zero Code Coverage.";
            xUnitKnowledge = "None";
        } else if (codeCoveragePercentage < 10)
            unitTestingSkills = "Very low coverage.";

        if (numberOfTestClasses == 1)
            if (totalTestLoC < 7)
                xUnitKnowledge = "None";
            else if (totalTestMethod == 1)
                xUnitKnowledge = "Poor";

        int longestClass = q.fetchHighestValue("javancss.xml", "//objects/object/ncss/text()");
        int longestMethod = q.fetchHighestValue("javancss.xml", "//functions/function/ncss/text()");
        int totalSingleLineComments = parseInt(q.fetchSpecificValue("javancss.xml", "//packages/total/single_comment_lines/text()"));
        int totalMultiLineComments = parseInt(q.fetchSpecificValue("javancss.xml", "//packages/total/multi_comment_lines/text()"));

        String codeSmells = "Code Smells";
        if (q.hasSmell("magic number"))
            codeSmells = "Magic Numbers";
        if (q.hasSmell("Unused import"))
            codeSmells += "<br>Dead Code";
        if (q.hasSmell("Found duplicate"))
            codeSmells += "<br>Duplicate Code";
        if (q.hasSmell("must be private and have accessor methods"))
            codeSmells += "<br>Indecent Exposure";
        if (longestClass > 60)
            codeSmells += "<br>Large Class";
        if (longestMethod > 10)
            codeSmells += "<br>Long Method";
        if (q.highestValueForError("Cyclomatic Complexity") > 6)
            codeSmells += "<br>Conditional Complexity";
        if (totalSingleLineComments > 1 || totalMultiLineComments > 1)
            codeSmells += "<br>Comments";

        String longestMethodMsg = msgLookUp(longestMethod, longMethodMsg);
        String longestTestMethodMsg = msgLookUp(longestTestMethod, longTestMethodMsg);

        int nPathComplexity = q.highestValueForError("NPath Complexity");
        int fanOutComplexity = q.highestValueForError("Fan-Out Complexity");

        String nPathComplexityMsg = msgLookUp(nPathComplexity, compexlityMsg);
        String fanOutComplexityMsg = msgLookUp(fanOutComplexity, compexlityMsg);

        result.append("\n<tr>\n<td>" + name + "</td>\n");
        result.append("<td>Overall Score</td>\n");
        result.append("<td>Program Meets Requirements</td>\n");
        result.append("<td>" + q.fetchSpecificValue("javancss.xml", "//packages/total/classes/text()") + "</td>\n");
        result.append("<td>" + q.fetchSpecificValue("javancss.xml", "//packages/total/ncss/text()") + "</td>\n");
        result.append("<td>" + longestMethodMsg + "</td>\n");
        result.append("<td>" + numberOfTestClasses + "</td>\n");
        result.append("<td>" + totalTestLoC + "</td>\n");
        result.append("<td>" + longestTestMethodMsg + "</td>\n");
        result.append("<td>" + xUnitKnowledge + "</td>\n");
        result.append("<td>" + codeSmells + "</td>\n");
        result.append("<td>OO Skills</td>\n");
        result.append("<td class='map_image'>" + q.fetchHighestValue("javancss.xml", "//functions/function/ccn/text()") + " <img src='" + path
                + "/results/reports/png/complexity-treemap.svg.png' alt='Cyclomatic Complexity'/></td>\n");
        result.append("<td>" + nPathComplexityMsg + "</td>\n");
        result.append("<td>" + fanOutComplexityMsg + "</td>\n");
        result.append("<td>" + unitTestingSkills + "</td>\n");
        result.append("<td class='map_image'>" + codeCoveragePercentage + "% <img src='" + path
                + "/results/reports/png/coverage-treemap.svg.png' alt='Code Coverage'/></td>\n");
        result.append("</tr>");
        writeFile(result.toString(), LOCATION, true);
    }

    private static String msgLookUp(int count, Map<Integer, String> mapping) {
        for (int threshold : mapping.keySet())
            if (count <= threshold)
                return count + " (" + mapping.get(threshold) + ")";
        return String.valueOf(count);
    }

    private static void writeFooter() throws IOException {
        writeFile("</table></body></html>", ".", true);
    }

    private static void writeHeader() throws IOException {
        String result = "<html><head>" +
                "<title>Preworkshop Assessment Report</title>" +
                "<style>table td {text-align: center} " +
                "\n br {mso-data-placement:same-cell;}" +
                "\n .map_image {width: 255px; height: 200px; vertical-align: top; } </style>" +
                "</head><body><table border='1'><tr>\n" +
                "<th>Name</th>\n" +
                "<th>Overall Score</th>\n" +
                "<th>Program Meets Requirements</th>\n" +
                "<th># of Classes</th>\n" +
                "<th>Total Code Size</th>\n" +
                "<th>Longest Method</th>\n" +
                "<th># of Test Cases</th>\n" +
                "<th>Total Code Size</th>\n" +
                "<th>Longest Test Method</th>\n" +
                "<th>Knowledge of xUnit</th>\n" +
                "<th>Code Smells in the code</th>\n" +
                "<th>Object Oriented Design Skill</th>\n" +
                "<th>Cyclomatic Complexity</th>\n" +
                "<th>NPath Complexity</th>\n" +
                "<th>Fan-Out Complexity</th>\n" +
                "<th>Quality of Unit Test</th>\n" +
                "<th>Test Coverage</th>\n" +
                "</tr>";
        writeFile(result, ".", false);
    }

    private static void writeFile(String content, String location, boolean append) throws IOException {
        File file = new File(location + "/" + "AssessmentReport.html");
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, append)));
        out.println(content);
        out.flush();
        out.close();
    }

    private boolean exists(String fileName, String xPath) throws Exception {
        Document doc = getDocument(fileName);
        XPathExpression expr = getXPathExpression(xPath);
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        return ((NodeList) result).getLength() > 0;
    }

    private int fetchHighestValue(String fileName, String xPath) throws Exception {
        Document doc = getDocument(fileName);
        XPathExpression expr = getXPathExpression(xPath);
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        int highestValue = 0;
        for (int i = 0; i < nodes.getLength(); i++) {
            String value = nodes.item(i).getNodeValue();
            int currentValue = extractFirstDigit(value);
            if (currentValue > highestValue)
                highestValue = currentValue;
        }
        return highestValue;
    }

    private static int extractFirstDigit(String value) {
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(value);
        int currentValue = 0;
        if (m.find(0))
            currentValue = Integer.parseInt(m.group(0));
        return currentValue;
    }

    private String fetchSpecificValue(String fileName, String xPath) throws Exception {
        Document doc = getDocument(fileName);
        XPathExpression expr = getXPathExpression(xPath);
        Object result = expr.evaluate(doc, XPathConstants.NODE);
        return ((Node) result).getNodeValue();
    }

    private XPathExpression getXPathExpression(String xPath) throws XPathExpressionException {
        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        return xpath.compile(xPath);
    }

    private Document getDocument(String fileName) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(true);
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        return builder.parse("results/rawmetrics/xml/" + fileName);
    }

    boolean hasSmell(String smell) throws Exception {
        return exists("checkstyle.xml", "//error[contains(@message, \"" + smell + "\")]/@message");
    }

    int highestValueForError(String error) throws Exception {
        return fetchHighestValue("checkstyle.xml", "//error[contains(@message, \"" + error + "\")]/@message");
    }
}

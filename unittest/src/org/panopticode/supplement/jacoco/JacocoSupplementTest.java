package org.panopticode.supplement.jacoco;

import junit.framework.TestCase;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.panopticode.PanopticodeClass;
import org.panopticode.PanopticodeMethod;
import org.panopticode.PanopticodeProject;
import org.panopticode.SupplementDeclaration;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.panopticode.TestHelpers.*;

public class JacocoSupplementTest extends TestCase {
    private JacocoSupplement supplement = new JacocoSupplement();

    public void testCoverageAppliesToInterface() {
        PanopticodeClass panopticodeClass;

        panopticodeClass = createDummyClass();
        panopticodeClass.setInterface(true);

        assertFalse(supplement.coverageAppliesTo(panopticodeClass));
    }
    public void testCoverageAppliesToMethod() {
        PanopticodeClass panopticodeClass;
        PanopticodeMethod panopticodeMethod;

        panopticodeClass = createDummyClass();

        panopticodeMethod = createDummyMethod();
        panopticodeMethod.setParentClass(panopticodeClass);

        assertTrue(supplement.coverageAppliesTo(panopticodeMethod));
    }

    public void testCoverageAppliesToAbstractMethod() {
        PanopticodeClass panopticodeClass;
        PanopticodeMethod panopticodeMethod;

        panopticodeClass = createDummyClass();

        panopticodeMethod = createDummyMethod();
        panopticodeMethod.setParentClass(panopticodeClass);
        panopticodeMethod.setAbstract(true);

        assertFalse(supplement.coverageAppliesTo(panopticodeMethod));
    }

    public void testCoverageAppliesToInterfaceMethod() {
        PanopticodeClass panopticodeClass;
        PanopticodeMethod panopticodeMethod;

        panopticodeClass = createDummyClass();
        panopticodeClass.setInterface(true);

        panopticodeMethod = createDummyMethod();
        panopticodeMethod.setParentClass(panopticodeClass);

        assertFalse(supplement.coverageAppliesTo(panopticodeMethod));
    }

    public void testCoverageAppliesToClass() {
        PanopticodeClass panopticodeClass;

        panopticodeClass = createDummyClass();

        assertTrue(supplement.coverageAppliesTo(panopticodeClass));
    }
}
